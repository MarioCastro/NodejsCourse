'use strict'

module.exports = function setupAgent (AgentModel) {

    function createOrUpdate(agent) {
        const cond = {
            where: {
                uuid: agent.uuid
            }
        };
        const existingAgnet = await AgentModel.findOne(cond);
        if (existingAgnet) {
            const updated = await AgentModel.update(agent, cond);
            return updated ? AgentModel.findOne(cond) : existingAgnet
        }
    
        const result = await AgentModel.create(agent);
        return result.toJSON();
    }
    function byId(id) {
        return AgentModel.ById(id);
    }

    return {
        byId,
        createOrUpdate
    }
}