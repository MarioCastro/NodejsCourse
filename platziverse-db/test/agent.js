'use strict'

const agent = {
    id: 1,
    uuid: 'yyy-yyy-yyy',
    name: 'Fixture',
    username: 'platzi',
    hostname: 'test-host',
    pid: 0,
    connected: true,
    createdAt: new Date(),
    updateAt: new Date()
};

const agents = [
    agent,
    extend(agent, {
        id: 2,
        uuid: 'zzz-yyy-yyy',
        name: 'Fixture 2',
        username: 'platzi2',
        hostname: 'test-host2',
        pid: 1,
        connected: false,
        createdAt: new Date(),
        updateAt: new Date()
    }),
    extend(agent, {
        id: 3,
        uuid: 'xxx-yyy-yyy',
        name: 'Fixture 3',
        username: 'platzi3',
        hostname: 'test-host3',
        pid: 2,
        connected: false,
        createdAt: new Date(),
        updateAt: new Date()
    }),
];

function extend(obj, values) {
    const clone = Object.assign({}, obj);
    return Object.assign(clone, values);
};

module.exports = {
    single: agent,
    all: agents,
    connected: agents.filter(a => a.connected),
    platzi: agents.filter(a => a.username === 'platzi'),
    byUuid: id => agents.filter(a => a.uuid === id).shift(),
    byId: id => agents.filter(a => a.id === id).shift()
};